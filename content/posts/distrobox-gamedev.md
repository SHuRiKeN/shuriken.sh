---
title: "Distrobox and Apx for Game deployment and testing"
date: 2023-09-24
draft: false
tags: ["distrobox","game-dev"]
---

![Distrobox Apx Logo](/images/distrobox-apx.png "Distrobox and Apx Logo")
Distrobox and Apx lets you use any linux distribution inside your terminal. However in certain ways it can be soo helpful for Game **Deployment** and **Testing**.

| This article focuses on Native Linux deployment and not through compatibility layers like Proton   |
|----------------------------------------------------------------------------------------------------|


## Game Deployment and testing ecosystem on Linux

Firstly I would like to touch on how the games are deployed on Linux, the caveats, because it will be important to understand a bit of context before diving into the solutions.

Most of the times deployers rely on their **Game Engines** to provide the export build or executable and ship their games on Steam, Itch, Flathub, their own Site etc. However when it comes to dependencies, they are either expected to be on the host system (example - when you install steam most if not all of the dependencies are installed automatically) or they are bundled with the build itself. Now we already see a few issues with both of these methods -

## The Problems

- In the first scenario where dependencies are expected to be on the host, this is mostly fine with games on steam which don't heavily rely on third party libraries/softwares apart from their engine which usually complies and works with those dependencies (99 percent of native games). However when they deploy such on Itch.io or their own websites, many run into the problem where users don't have required dependencies installed and then they have to either bundle them manually or give instructions to users on how to install them for each distro and in both scenarios they will need to **figure out the dependencies for each distro and even for each user** (this is because of variation between each user having different set of packages installed). And for a AAA game who wants to deploy a native build this can be a massive massive uptaking.

- In the second scenario even tho dependencies are bundled, use of third party libraries and softwares outside the engine may lead to bundled dependencies by the Game Engine to be lacking or unaware of what is needed by certain parts of your games to function. And then we lead to the same cycle as explained in the first scenario.

- In both of the above scenarios there also can be an issue of **dependency versioning**. Distros having different versions of the same package might lead to unexpected bugs, misbehaviour. Which can be a nightmare for Game testers. As this means many distro specific and user specific bugs.

## The ultimate solution - Why not Flatpaks !!!?

Flatpaks in my opinion solves all of the issues above - 

- Have a single runtime that you can target and test on which will work identical on every distro !
- Has automated deployment ! once done can be easily maintained.
- Can give you a single built file which you can even upload on Itch.io or your website so users can install it with a single click. **flatpakref**
- Be assured that dependencies versioning is same across all users, as you are targetting a single runtime/ dependencies you included. 

**So no user specific bugs, no figuring out user specific dependencies, assured all users have the exact same build, easy to maintain.. where is the issue ?? Well its adoption on both on user level (improving rapidly), Steam, lack of runtimes**

- Currently steam or itch.io do not consider flatpak as a dependency and do not work well with a flatpak. (not talking about steam flatpak but game as a flatpak on steam). Even tho on itch you can deploy a flatpak file !
- Game Engines don't have a flatpak runtime or an export build users can easily pickup (other runtimes can still be used and deps can be bundled), however having a environment specific runtime always helps !
- Not all users / distros use flatpak.

- As a sidenote people using **Godot** there is a flatpak runtime they can use - [Godot Engine BaseApp](https://github.com/flathub/org.godotengine.godot.BaseApp) 

## Distrobox and Apx 

Even tho I believe Flatpaks are the ultimate solution, currently we have to go by "it is what it is" philosophy and work with what we have. So we have different users on different distros having different package versions and an export build provided by our Game Engine. And spoiler alert we can create this whole scenario on a single system using Distrobox or Apx (built on distrobox, using it under the hood making it easier to use). Because we have to replicate the problem on our system in order to solve it.

- Allows us to quickly fire up any distro container.
- Can fire up multiple distro containers to replicate user environments.
- Allows us to figure out dependencies required for each distro.
- As the containers themselves are super minimal, we can be sure users already have those packages and can figure out a dependency list on top of that. 
- Help us to test and replicate most distro specific bugs.
- Helps us to replicate user specific bugs by replicating their environment on a single system.
- Deployers can then bundle the dependencies that are causing dependency version specific issues.

Like this these containers can help us replicating the exact environment we need on a single system, it might still leave some very low level stuff behind like Kernel specific issue and others but those don't happen 99.99999 percent of the times.

---
title: "About Me"
draft: false
---

___
*So, Surely with hardship comes ease*

*--- Quran 94:5*
___

My name is **Jatin**. I'm known as **SHuRiKeN**, I am currently 21 years old.

I love tinkering with systems, and I like contributing to **Free and Open Source Software**; Big fan of **Linux** and projects like **Godot**, **Blender**. When it comes to my workflow I really like immutable Linux distros and utilizing containers - **Flatpak**, **Distrobox**, **Podman**. Below you can check softwares I use.

I like reading books that improve me Psychologically - Jordan B. Peterson's 12 Rules of Life, Beyond Order; The Clear Quran; are few examples. I also like playing games mostly RTS, various MMORPGs and Indie Games. I also hit the gym daily, so I can be the best version of myself and keep myself disciplined and self controlled.

## Softwares I use

**Operating System** - My own custom OS image based on Silverblue - [ShurikenOS](https://github.com/shuriken1812/ShurikenOS)  
**Desktop Environment** - [Gnome](https://gnome.org)  
**Terminal and Shell** - [Blackbox](https://gitlab.gnome.org/raggesilver/blackbox) and [fish](https://fishshell.com/)  
**Code Editor / Dev Environment** - [Neovim](https://neovim.io/) + [NvChad](https://NvChad.com) + [Tmux](https://github.com/tmux/tmux/wiki)  
**Primary Game Dev and related tools** - [Godot](https://godotengine.org) + [Blender](https://blender.org) + [Pixelorama](https://orama-interactive.itch.io/pixelorama) + [Gimp](https://gimp.org)  
**Web Browser** - [Firefox with Custom user.js]  
**Matrix Client** - [SchildiChat](https://schildi.chat/)  
**Discord Client** - [Armcord](https://armcord.app/)

## My Socials

You can message me on [Discord](https://discord.com/usrs/489575472024256512) or [Matrix](https://matrix.to/#/@shuriken:fedora.im)  
You can send me an [Email](mailto:shuriken@disroot.org)  
You can follow me on [Mastodon](https://fosstodon.org/@shuriken) and [X](https://twitter.com/SHuRiKeN1812)  
You can view my projects and contributions on [Disroot Git](https://git.disroot.org/SHuRiKeN) (Main) and [Github](https://github.com/shuriken1812) (for contributions, github actions & mirror to my main)
